## Overview
This is example of deploying Sonar monitoring agent on Windows with Ansible role.

## Prerequisites
The following are required

* Ansible version 2.7+
* [LeapfrogOnline](https://www.leapfrogonline.io) [merge_vars](https://github.com/leapfrogonline/ansible-merge-vars) module for Ansible.

## Steps
1. Install pre-requisites and clone this repository.
2. Run the following command in playbook directory:

```bash
ansible-playbook -i inventory.yml -s run.yml 
```

## Considerations

1. Role can be modified to deploy additional files (aka "arrays") with metrics defined by administrators.
2. Download URI may be modified in for pointing to agent archive binaries in other locations.
3. Inventory and global_vars settings can be modified for additional servers and WinRM security for Ansible.